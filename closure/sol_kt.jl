using OrdinaryDiffEq, KitBase, Plots
using KitBase.JLD2
using Base.Threads: @threads

function df!(dh, db, h, b, fhL, fbL, fhR, fbR, vs, gas, Δx)
    w = moments_conserve(h, b, vs.u, vs.weights, KB.VDF{2,1})
    prim = conserve_prim(w, gas.γ)
    MH = maxwellian(vs.u, prim)
    MB = energy_maxwellian(MH, prim, gas.K)
    τ = vhs_collision_time(prim, gas.μᵣ, gas.ω)

    for i in eachindex(dh)
        dh[i] = (fhL[i] - fhR[i]) / Δx + (MH[i] - h[i]) / τ
        db[i] = (fbL[i] - fbR[i]) / Δx + (MB[i] - b[i]) / τ
    end

    return nothing
end

function rhs!(df, f, p, t)
    ps, vs, gas = p

    nu = size(f, 1)
    nx = size(f, 3)

    flux = zeros(nu, 2, nx + 1)
    @threads for j in 2:nx
        fh = @view flux[:, 1, j]
        fb = @view flux[:, 2, j]
        flux_kfvs!(
            zeros(3),
            fh,
            fb,
            f[:, 1, j-1],
            f[:, 2, j-1],
            f[:, 1, j],
            f[:, 2, j],
            vs.u,
            vs.weights,
            1.0,
        )
    end

    @threads for j in 2:(nx-1)
        dh = @view df[:, 1, j]
        db = @view df[:, 2, j]
        df!(
            dh,
            db,
            f[:, 1, j],
            f[:, 2, j],
            flux[:, 1, j],
            flux[:, 2, j],
            flux[:, 1, j+1],
            flux[:, 2, j+1],
            vs,
            gas,
            ps.dx[j],
        )
    end

    return nothing
end

ps = PSpace1D(0, 1, 100)
vs = VSpace1D(-5, 5, 48)
gas = Gas(; Kn=1e-3, K=2.0, γ=5 / 3)

w0 = zeros(3, ps.nx)
prim0 = zeros(3, ps.nx)
f0 = zeros(vs.nu, 2, ps.nx)
for i in 1:ps.nx
    if ps.x[i] <= 0.5
        prim0[:, i] .= [1.0, 0.0, 0.5]
    else
        prim0[:, i] .= [0.3, 0.0, 0.625]
    end

    w0[:, i] .= prim_conserve(prim0[:, i], gas.γ)
    f0[:, 1, i] .= maxwellian(vs.u, prim0[:, i])
    f0[:, 2, i] .= energy_maxwellian(f0[:, 1, i], prim0[:, i], gas.K)
end

tspan = (0.0, 0.16)
δ = ps.dx[1] / 5 * 10
tsteps = tspan[1]:δ:tspan[end]
p = (ps, vs, gas)
prob = ODEProblem(rhs!, f0, tspan, p)
res = solve(prob, Midpoint(); saveat=tsteps)
sol = res |> Array

resArr = zeros(3, ps.nx, length(tsteps))
for j in 1:length(tsteps)
    for i in 1:ps.nx
        f = res.u[j]
        w = moments_conserve(f[:, 1, i], f[:, 2, i], vs.u, vs.weights, KB.VDF{2,1})
        resArr[:, i, j] .= w
    end
end

@save "sod_ktsol.jld2" resArr
