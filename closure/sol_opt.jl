"""
Machine learning assisted modeling for hydrodynamic closure

"""

using OrdinaryDiffEq, KitBase, Solaris, Plots
using EnsembleKalmanProcesses, Random, Statistics
using EnsembleKalmanProcesses.ParameterDistributions:
    constrained_gaussian, combine_distributions
using KitBase.JLD2, KitBase.ProgressMeter
using Flux: sigmoid
using LinearAlgebra: I
using Base.Threads: @threads

function flux_basis(wL, wR)
    primL = conserve_prim(wL, gas.γ)
    primR = conserve_prim(wR, gas.γ)
    Mu1, Mx1, MuL1, MuR1 = KB.gauss_moments(primL, 2)
    Mu2, Mx2, MuL2, MuR2 = KB.gauss_moments(primR, 2)
    MuvL = moments_conserve(MuL1, Mx1, 1, 0)
    MuvR = moments_conserve(MuR2, Mx2, 1, 0)
    fw = primL[1] * MuvL + primR[1] * MuvR

    return fw
end

cd(@__DIR__)
@load "sod_ktsol.jld2" resArr

nn = FnChain(FnDense(3, 12, tanh; bias=false), FnDense(12, 3, tanh; bias=false))
p0 = init_params(nn)

function rhs0!(dw, w, p, t)
    nx = size(w, 2)

    flux = zeros(3, nx + 1)
    for j in 2:nx
        flux[:, j] .= flux_basis(w[:, j-1], w[:, j])
    end

    for j in 2:(nx-1)
        for i in 1:3
            dw[i, j] = (flux[i, j] - flux[i, j+1]) / ps.dx[j]
        end
    end

    return nothing
end
function rhs!(dw, w, p, t)
    nx = size(w, 2)

    flux = zeros(3, nx + 1)

    for j in 2:nx
        flux[:, j] .= flux_basis(w[:, j-1], w[:, j])
        #flux[:, j] .*= nn(vcat(w[:, j-1], w[:, j]), p)
        flux[:, j] .*= (1.0 .+ nn((w[:, j-1] .- w[:, j]), p))
    end
    for j in 2:(nx-1)
        for i in 1:3
            dw[i, j] = (flux[i, j] - flux[i, j+1]) / ps.dx[j]
        end
    end

    return nothing
end

ps = PSpace1D(0, 1, 100)
gas = Gas(; Kn=1e-3, K=2.0, γ=5 / 3)

w0 = zeros(3, ps.nx)
prim0 = zeros(3, ps.nx)
for i in 1:ps.nx
    if ps.x[i] <= 0.5
        prim0[:, i] .= [1.0, 0.0, 0.5]
    else
        prim0[:, i] .= [0.3, 0.0, 0.625]
    end

    w0[:, i] .= prim_conserve(prim0[:, i], gas.γ)
end

tspan = (0.0, 0.16)
δ = ps.dx[1] / 5 * 10
tsteps = tspan[1]:δ:tspan[end]

prob0 = ODEProblem(rhs0!, w0, tspan, ())
sol0 = solve(prob0, Midpoint(); saveat=tsteps)
solArr0 = Array(sol0)

function loss(p)
    prob = ODEProblem(rhs!, w0, tspan, p)
    sol = try
        solve(prob, Midpoint(); saveat=tsteps) |> Array
    catch
        return NaN
    end
    l = sum(abs2, sol .- resArr)

    return l
end

loss(p0)

#@load "ensemble.jld2" ekp
#---
begin
    dim_output = 1
    stabilization_level = 1e-3
    Γ = stabilization_level * Matrix(I, dim_output, dim_output)
    loss_target = [0.0]

    prior_u =
        [constrained_gaussian("u" * string(iter), 0, 1, -Inf, Inf) for iter in 1:length(p0)]
    prior = combine_distributions(prior_u)

    N_ensemble = 100
    N_iterations = 10

    rng = Random.seed!(Random.GLOBAL_RNG, 41)
    initial_ensemble = construct_initial_ensemble(rng, prior, N_ensemble)
    ekp = EnsembleKalmanProcess(
        initial_ensemble,
        loss_target,
        Γ,
        #Inversion(),
        TransformInversion();
        scheduler=DefaultScheduler(),
        accelerator=DefaultAccelerator(),
        localization_method=EnsembleKalmanProcesses.Localizers.NoLocalization(),
        failure_handler_method=SampleSuccGauss(),
    )
end

pe0 = get_u_mean_final(ekp)
loss(pe0)

for iter in 1:(N_iterations÷2)
    println("iteration $iter")
    params_i = get_u_final(ekp)
    g_ens = zeros(1, N_ensemble)
    @showprogress @threads for i in 1:N_ensemble
        g = loss(params_i[:, i])
        g_ens[1, i] = g
    end
    update_ensemble!(ekp, g_ens)

    res = get_u_final(ekp)
    pt = [mean(res[iter, :]) for iter in 1:length(p0)]
    @show loss(pt)
end
#---

p = get_u_mean_final(ekp)
loss(p)

prob = ODEProblem(rhs!, w0, tspan, p)
sol = solve(prob, Midpoint(); p=p, saveat=tsteps)
solArr = Array(sol)
let itx = 1
    scatter(solArr[itx, :, end]; label="current", alpha=0.4)
    plot!(solArr0[itx, :, end]; lw=1.5, label="Navier-Stokes")
    plot!(resArr[itx, :, end]; lw=1.5, label="kinetic")
end
#savefig("sod.png")

#@save "ensemble.jld2" ekp
