using DifferentiationInterface
using Enzyme, Zygote
const DI = DifferentiationInterface

"""
We test the function f=x³ + xy², which has ∂ₓf=3x²+y², ∂ᵧf=2xy, ∂ₓₓf=6x, ∂ᵧᵧf=2
inserting (x=2, y=2) yields ∂ₓf=16, ∂ᵧf=8, ∂ₓₓf=12, ∂ᵧᵧf=4
"""
function f(x::Array{Float64}, y::Array{Float64})
    y[1] = x[1]^3 + x[2]^2 * x[1]
    return nothing
end

#--- 1st order ---#
begin
    x = [2.0, 2.0]
    bx = [0.0, 0.0]
    y = [0.0]
    by = [1.0] # seed that has to be set to 1.0 in order to compute the gradient via x̄ = ȳ ⋅ ∇f(x)
end
Enzyme.autodiff(Reverse, f, Duplicated(x, bx), Duplicated(y, by))
@show bx

#--- 2nd order ---#
begin # see https://enzymead.github.io/Enzyme.jl/stable/generated/autodiff/#Forward-over-reverse for formulations
    y = [0.0]
    x = [2.0, 2.0]

    dy = [0.0] # ȳ̇
    dx = [1.0, 0.0] # ẋ

    bx = [0.0, 0.0]
    by = [1.0] # ȳ
    dbx = [0.0, 0.0]
    dby = [0.0]
end

Enzyme.autodiff(
    Forward,
    (x, y) -> Enzyme.autodiff_deferred(Reverse, f, x, y),
    Duplicated(Duplicated(x, bx), Duplicated(dx, dbx)),
    Duplicated(Duplicated(y, by), Duplicated(dy, dby)),
)
@show dbx
