"""
https://juliagraphs.org/GraphNeuralNetworks.jl/docs/GNNLux.jl/stable/tutorials/gnn_intro/
https://colab.research.google.com/drive/1h3-vJGRVloF5zStxL5I0rSy4ZUPNsjy8?usp=sharing#scrollTo=qoW2Z7P70LNQ
"""

using MLDatasets, Lux, GNNLux, Random
using LinearAlgebra, OneHotArrays, Statistics
import CairoMakie as Makie
import GraphMakie

ENV["DATADEPS_ALWAYS_ACCEPT"] = "true" # don't ask for dataset download confirmation
rng = Random.seed!(10)

# This graph describes a social network of 34 members of a karate club 
# and documents links between members who interacted outside the club. 
# We are interested in detecting communities that arise from the member's interaction.
dataset = MLDatasets.KarateClub()

# We can see that this dataset holds one graph.
# The graph holds 4 classes, which represent the community each node belongs to.
dataset[1].node_data.labels_comm

# Transform to GNNLux graph
g = mldataset2gnngraph(dataset)

x = zeros(Float32, g.num_nodes, g.num_nodes)
x[diagind(x)] .= 1

# It describes for which nodes we already know their community assigments.
# In total, we are only aware of the ground-truth labels of 4 nodes (one for each community).
# The task is to infer the community assignment for the remaining nodes.
train_mask = [
    true, # √
    false,
    false,
    false,
    true, # √
    false,
    false,
    false,
    true, # √
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    true, # √
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
]

labels = g.ndata.labels_comm
y = onehotbatch(labels, 0:3)

g = GNNGraph(g; ndata=(; x, y, train_mask))

begin
    println("Number of nodes: $(g.num_nodes)")
    println("Number of edges: $(g.num_edges)")
    println("Average node degree: $(g.num_edges / g.num_nodes)")
    println("Number of training nodes: $(sum(g.ndata.train_mask))")
    println("Training node label rate: $(mean(g.ndata.train_mask))")
    println("Has self-loops: $(has_self_loops(g))")
    println("Is undirected: $(is_bidirected(g))")
end

GraphMakie.graphplot(g; node_size=20, node_color=labels, arrow_show=false)
GraphMakie.graphplot(g |> to_unidirected; node_size=20, node_color=labels, arrow_show=false)
