using DifferentiationInterface
using Enzyme, Zygote
const DI = DifferentiationInterface

f(x) = @. x^3

xs = 2.0
xv = [2.0, 3.0]

DI.derivative(f, AutoEnzyme(), xs)
DI.derivative.(f, AutoEnzyme(), xv)
