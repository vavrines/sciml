import torch
import numpy
from collections import OrderedDict


class DNN(torch.nn.Module):
    def __init__(self, layers):
        super(DNN, self).__init__()

        # parameters
        self.depth = len(layers) - 1

        # set up layer order dict
        self.activation = torch.nn.Tanh

        layer_list = list()
        for i in range(self.depth - 1):
            layer_list.append(
                ("layer_%d" % i, torch.nn.Linear(layers[i], layers[i + 1]))
            )
            layer_list.append(("activation_%d" % i, self.activation()))

        layer_list.append(
            ("layer_%d" % (self.depth - 1), torch.nn.Linear(layers[-2], layers[-1]))
        )
        layerDict = OrderedDict(layer_list)

        # deploy layers
        self.layers = torch.nn.Sequential(layerDict)

    def forward(self, x):
        out = self.layers(x)
        return out


class PhysicsInformedNN:
    def __init__(self, X_u, u, X_f, layers, lb, ub, nu, device = "cpu"):
        '''
        X_u: reference grid
        u: reference solution
        X_f: unsupervised grid
        lb: lower boundary
        ub: upper boundary
        nu: viscosity in the Burgers' equation
        '''
        # boundary conditions
        self.lb = torch.tensor(lb).float().to(device)
        self.ub = torch.tensor(ub).float().to(device)

        # data
        self.x_u = torch.tensor(X_u[:, 0:1], requires_grad=True).float().to(device)
        self.t_u = torch.tensor(X_u[:, 1:2], requires_grad=True).float().to(device)
        self.x_f = torch.tensor(X_f[:, 0:1], requires_grad=True).float().to(device)
        self.t_f = torch.tensor(X_f[:, 1:2], requires_grad=True).float().to(device)
        self.u = torch.tensor(u).float().to(device)

        self.layers = layers
        self.nu = nu

        # deep neural networks
        self.dnn = DNN(layers).to(device)

        # optimizers: using the same settings
        self.optimizer = torch.optim.LBFGS(
            self.dnn.parameters(),
            lr=1.0,
            max_iter=2000,
            max_eval=2000,
            history_size=50,
            tolerance_grad=1e-5,
            tolerance_change=1.0 * numpy.finfo(float).eps,
            line_search_fn="strong_wolfe",  # can be "strong_wolfe"
        )

        self.iter = 0
        self.device = device

    # compute solution
    def predict(self, x, t):
        u = self.dnn(torch.cat([x, t], dim=1))
        return u

    # compute residual of equation
    def residual(self, x, t):
        u = self.predict(x, t)

        u_t = torch.autograd.grad(
            u, t, grad_outputs=torch.ones_like(u), retain_graph=True, create_graph=True
        )[0]
        u_x = torch.autograd.grad(
            u, x, grad_outputs=torch.ones_like(u), retain_graph=True, create_graph=True
        )[0]
        u_xx = torch.autograd.grad(
            u_x,
            x,
            grad_outputs=torch.ones_like(u_x),
            retain_graph=True,
            create_graph=True,
        )[0]

        f = u_t + u * u_x - self.nu * u_xx
        return f

    def loss(self):
        self.optimizer.zero_grad()

        u_pred = self.predict(self.x_u, self.t_u)
        f_pred = self.residual(self.x_f, self.t_f)
        loss_u = torch.mean((self.u - u_pred) ** 2)
        loss_f = torch.mean(f_pred**2)

        loss_t = loss_u + loss_f

        loss_t.backward()
        self.iter += 1
        if self.iter % 100 == 0:
            print(
                "Iter %d, Loss: %.5e, Loss_u: %.5e, Loss_f: %.5e"
                % (self.iter, loss_t.item(), loss_u.item(), loss_f.item())
            )
        return loss_t

    def train(self):
        self.dnn.train()

        # Backward and optimize
        self.optimizer.step(self.loss)

    def output(self, X):
        x = torch.tensor(X[:, 0:1], requires_grad=True).float().to(self.device)
        t = torch.tensor(X[:, 1:2], requires_grad=True).float().to(self.device)

        self.dnn.eval()
        u = self.predict(x, t)
        f = self.residual(x, t)
        u = u.detach().cpu().numpy()
        f = f.detach().cpu().numpy()
        return u, f
