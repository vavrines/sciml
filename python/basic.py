# %%
import torch
import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from pyDOE import lhs
from scipy.interpolate import griddata
import matplotlib.gridspec as gridspec
from model import *

np.random.seed(1234)

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")


# %%
nu = 0.01 / np.pi
noise = 0.0

N_u = 100
N_f = 10000
layers = [2, 20, 20, 20, 20, 20, 20, 20, 20, 1]
# %%
nn = DNN(layers).to(device)

# %%
x = np.random.rand(2, 4)
xt = torch.tensor(x).float().to(device)  # float32 tensor
# %%
nn(xt)
# %%
