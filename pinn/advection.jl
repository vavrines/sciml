"""
linear advection equation ∂ₜu + a∂ₓu = 0
"""

using Lux, Solaris, LinearAlgebra, Plots, CUDA
using KitBase: meshgrid, mat_split
import DifferentiationInterface as DI
using Solaris.Flux: DataLoader, cpu, gpu, throttle

device = cpu

tsteps = range(0.0; step=0.01, length=10) |> collect
xsteps = range(-1.0, 1.0; length=256) |> collect
tm, xm = meshgrid(tsteps, xsteps)
xMesh1D = reshape(xm, (1, :))
tMesh1D = reshape(tm, (1, :))
X = cat(xMesh1D, tMesh1D; dims=1) .|> Float32
Y = zeros(Float32, 1, size(X, 2))
for j in axes(Y, 2)
    Y[1, j] = sin(π * (X[1, j] - X[2, j]) + π)
end
train_loader = DataLoader((X, Y); batchsize=256, shuffle=false) |> device

nn = Chain(
    Dense(2, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 20, tanh),
    Dense(20, 1),
)
p0, st = Solaris.setup(nn)

# initial condition
X0 = X[:, 1:256] |> device
Y0 = Y[:, 1:256] |> device

# boundary condition
xl = -1 .* ones(Float32, 1, length(tsteps))
XL = vcat(xl, tsteps')
xr = 1 .* ones(Float32, 1, length(tsteps))
XR = vcat(xr, tsteps')

_l = train_loader.batchsize
mdx = zeros(Float32, 2 * _l, 1)
mdt = zeros(Float32, 2 * _l, 1)
for i in 1:_l
    mdx[2(i-1)+1] = 1.0f0
    mdt[2i] = 1.0f0
end
mdx = mdx |> device
mdt = mdt |> device

function loss(p, dl)
    x, y = dl
    model = SR.stateful(nn, p, st) |> device
    #u = model(x) # we don't need function value here

    # equation
    jac = DI.jacobian(model, AutoForwardDiff(), x)
    ux = jac * mdx |> permutedims
    ut = jac * mdt |> permutedims
    l1 = @. ut + 1.0 * ux

    # initial condition
    u0 = model(X0)
    l2 = u0 - Y0

    # boundary condition
    uL = model(XL)
    uR = model(XR)
    l3 = uL - uR

    loss = sum(abs2, l1) + sum(abs2, l2) + sum(abs2, l3)

    return loss
end

res = sci_train(
    loss,
    p0,
    train_loader,
    AdamW(0.05);
    device=device,
    cb=default_callback,
    maxiters=20,
    ad=AutoZygote(),
)
res = sci_train(
    loss,
    res.u,
    train_loader,
    AdamW(0.01);
    device=device,
    cb=default_callback,
    maxiters=40,
    ad=AutoZygote(),
)

let idx = 10
    Xtest = hcat(xm[:, idx], tm[:, idx]) |> permutedims
    Ytest = nn(Xtest, cpu(res.u), st)[1] |> permutedims

    _t = Xtest[2, :] |> first
    Yref = @. sin(π * (Xtest[1, :] - _t) + π)

    plot(xm[:, idx], Yref; label="exact")
    plot!(xm[:, idx], Ytest; label="NN")
end

# write solution
using Solaris.JLD2
cd(@__DIR__)
u = cpu(res.u)
@save "advection.jld2" u
