using Solaris, Plots, Random
using Solaris.Lux
using Solaris.Zygote

nx = 100
X = collect(range(0, 1; length=nx)) |> permutedims

ann = FnChain(FnDense(1, 20, tanh), FnDense(20, 1))
p0 = init_params(ann)

function loss(p)
    u(x) = 1 .+ ann(x, p) .* x
    ux(x) = Zygote.pullback(u, x)[2](ones(size(x)))[1]

    pred = u(X) .+ ux(X)
    loss = sum(abs2, pred)

    return loss
end

res = sci_train(loss, p0, Adam(0.05); cb=default_callback, maxiters=200, ad=AutoZygote())
res = sci_train(loss, res.u, LBFGS(); cb=default_callback, maxiters=200, ad=AutoZygote())

xTest = Vector(range(0.0, 1.0; length=33)) |> permutedims
yTest = exp.(-xTest)
yPred = 1 .+ ann(xTest, res.u) .* xTest
plot(xTest', yTest'; label="exact")
scatter!(xTest', yPred'; label="NN")
