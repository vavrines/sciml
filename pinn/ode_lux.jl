"""
Lux v0.5.41 doesn't work well with nested Zygote.pullback,
see https://github.com/LuxDL/Lux.jl/issues/600.
So we modify our codes to work around it.
"""

using Solaris, Plots, Random, LinearAlgebra
using Solaris.Lux

nx = 100
X = collect(range(0, 1; length=nx)) |> permutedims
Y = zeros(axes(X))

nn = Chain(
    Dense(1 => 20, tanh; use_bias=false),
    Dense(20 => 20, tanh; use_bias=false),
    Dense(20 => 1; use_bias=false),
)
ps, st = Lux.setup(Xoshiro(0), nn)

function loss(p)
    model = StatefulLuxLayer{true}(nn, p, st)
    u = 1 .+ model(X)
    ux = Zygote.jacobian(model, X)[1] |> diag |> permutedims # ∂u = ∂model

    pred = u .+ ux
    l = sum(abs2, pred)

    return l
end

res = sci_train(loss, ps, AdamW(); cb=default_callback, maxiters=200, ad=AutoZygote())
res = sci_train(loss, res.u, LBFGS(); cb=default_callback, maxiters=200, ad=AutoZygote())

xTest = Vector(range(0.0, 1.0; length=33)) |> permutedims
yTest = exp.(-xTest)
yPred = 1 .+ nn(xTest, res.u, st)[1]
plot(xTest', yTest'; label="exact")
scatter!(xTest', yPred'; label="NN")
