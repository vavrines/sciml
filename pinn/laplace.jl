using Solaris, Plots
using Solaris.Lux
using Solaris.Zygote
using Optim: LBFGS
using Optimization: AutoZygote
using KitBase: meshgrid, mat_split

xspan = 0.0f0:0.1f0:2.0f0
yspan = 0.0f0:0.1f0:1.0f0

x = collect(xspan)
y = collect(yspan)
nx = length(x)
ny = length(y)
xMesh, yMesh = meshgrid(x, y)
xMesh1D = reshape(xMesh, (1, :))
yMesh1D = reshape(yMesh, (1, :))
mesh = cat(xMesh1D, yMesh1D; dims=1)

X = deepcopy(mesh)
Y = zeros(Float32, 1, length(x) * length(y))

m = Chain(Dense(2, 20, tanh), Dense(20, 20, tanh), Dense(20, 1))
p0, st = Solaris.setup(m)

mat_x(x) = [1.0 0.0] * x
mat_y(x) = [0.0 1.0] * x

function loss(p)
    u(x) =
        sin.(π .* mat_split(x)[1] ./ 2.0f0) .* mat_split(x)[2] .+
        mat_split(x)[1] .* (2.0f0 .- mat_split(x)[1]) .* mat_split(x)[2] .*
        (1.0f0 .- mat_split(x)[2]) .* m(x, p, st)[1]

    ux(x) = pullback(u, x)[2](ones(size(x)))[1] |> mat_x
    uy(x) = pullback(u, x)[2](ones(size(x)))[1] |> mat_y

    uxx(x) = pullback(ux, x)[2](ones(size(x)))[1] |> mat_x
    uyy(x) = pullback(uy, x)[2](ones(size(x)))[1] |> mat_y

    pred = uxx(X) .+ uyy(X)
    loss = sum(abs2, pred)

    return loss
end

loss(p0)

# debug
u(x) =
    sin.(π .* mat_split(x)[1] ./ 2.0f0) .* mat_split(x)[2] .+
    mat_split(x)[1] .* (2.0f0 .- mat_split(x)[1]) .* mat_split(x)[2] .*
    (1.0f0 .- mat_split(x)[2]) .* m(x, p0, st)[1]

ux(x) = pullback(u, x)[2](ones(size(x)))[1] |> mat_x
uy(x) = pullback(u, x)[2](ones(size(x)))[1] |> mat_y

uxx(x) = pullback(ux, x)[2](ones(size(x)))[1] |> mat_x
uyy(x) = pullback(uy, x)[2](ones(size(x)))[1] |> mat_y

u(X)
ux(X)
uxx(X) # doesn't work
