using Solaris,
    Plots, ComponentArrays, Random, Zygote, DifferentiationInterface, LinearAlgebra
using Solaris.Lux
using Optimisers: Adam
using Optim: LBFGS

const DI = DifferentiationInterface

nx = 100
X = collect(range(0, 1; length=nx)) |> permutedims

#--- Solaris ---#
ann = FnChain(FnDense(1, 20, tanh), FnDense(20, 1))
p0 = init_params(ann)

function loss(p)
    u(x) = 1 .+ ann(x, p) .* x

    #ux(x) = begin
    #    t = DI.derivative.(u, AutoZygote(), x) # AutoEnzyme() doesn't work now
    #    #t = DI.derivative.(u, AutoEnzyme(), x)
    #    [t[i, j][1] for i in axes(x, 1), j in axes(x, 2)]
    #end # principly work but not now

    ux(x) = begin
        t = DI.jacobian(u, AutoZygote(), x)
        t |> diag |> permutedims
    end

    pred = u(X) .+ ux(X)
    loss = sum(abs2, pred)

    return loss
end

cb = function (θ, l)
    display(l)
    return false
end

res = sci_train(loss, p0, Adam(0.05); cb=cb, maxiters=200, ad=AutoForwardDiff()) # nested Zygote doesn't work so we switch to ForwardDiff
res = sci_train(loss, res.u, LBFGS(); cb=cb, maxiters=200, ad=AutoForwardDiff())

xTest = Vector(range(0.0, 1.0; length=33)) |> permutedims
yTest = exp.(-xTest)
yPred = 1 .+ ann(xTest, res.u) .* xTest
plot(xTest', yTest'; label="exact")
scatter!(xTest', yPred'; label="NN")
