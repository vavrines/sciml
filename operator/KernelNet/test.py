# %%
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
import kernelnet as kn
import time
import sys

def rse(y_true, y_pred, quadrature=None):
    d = tf.einsum(
        "...i,...i->...", (y_true[..., 0] - y_pred[..., 0]) ** 2, quadrature[1, :]
    )
    a = tf.einsum("...i,...i->...", y_true[..., 0] ** 2, quadrature[1, :])
    l = tf.sqrt(d / a)
    #tf.print("\n loss:", l, output_stream=sys.stdout)
    return l

# %%
d = 1
Nv = 101
vmax = 1.0
vs = np.linspace(-vmax, vmax, Nv, dtype=np.float32)
ws = np.concatenate(([1.0 / 2], np.ones(Nv - 2), [1.0 / 2]), dtype=np.float32)
quad = tf.stack([vs, ws], name="Quadrature data")

# training hyperparameters
epochs = 100
steps_per_epoch = 2
batch_size = int(epochs / 100)

shuffle_size = steps_per_epoch * batch_size
sample_size = epochs * steps_per_epoch * batch_size

# learning rate
lr = 0.001

# %%
ds = tf.data.Dataset.from_generator(
    lambda: kn.__generate_data(vs, ws),
    output_signature=(
        tf.TensorSpec(shape=vs.shape, dtype=tf.float32),
        tf.TensorSpec(shape=(Nv, 2), dtype=tf.float32),
    ),
)

train_ds = (
    ds.take(sample_size)
    .shuffle(shuffle_size, reshuffle_each_iteration=True)
    .batch(batch_size)
)

val_ds = (
    ds.take(sample_size)
    .shuffle(shuffle_size, reshuffle_each_iteration=True)
    .batch(batch_size)
)
# %%
pred_ds = ds.take(sample_size)
f_pred = np.zeros(shape=(0, Nv))
q_true = np.zeros(shape=(0, Nv))
for s in list(pred_ds.as_numpy_iterator()):
    f_pred = np.vstack((f_pred, s[0]))
    q_true = np.vstack((q_true, s[1][:, 0]))

# %%
model_filename = "kernelnet-" + str(time.time()) + ".keras"
model = kn.kernelnet(quad)
optimizer = keras.optimizers.Adam(learning_rate=lr)
model.compile(
    optimizer=optimizer,
    loss=keras.losses.MeanSquaredError(),
    metrics=["accuracy"],
)
model.fit(  # x_train, y_train,
    train_ds,
    # validation_split=0.5,
    steps_per_epoch=steps_per_epoch,
    epochs=epochs,
)

# %%
model.fit(  # x_train, y_train,
    train_ds,
    # validation_split=0.5,
    steps_per_epoch=steps_per_epoch,
    epochs=1000,
)

# %%
q_pred = model.predict(f_pred)

# %%
ind = 3
plt.figure()
plt.subplot(1, 2, 1)
plt.plot(vs, f_pred[ind, :])
plt.title("f")
plt.subplot(1, 2, 2)
plt.plot(vs, q_true[ind, :], label="True")
plt.plot(vs, q_pred[ind, :, 0], label="Prediction")
plt.legend(loc="best")
plt.title("Q")
plt.show()
# %%


model.loss(q_true, q_pred)
# %%
