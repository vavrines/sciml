#!/usr/bin/python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser

import tensorflow as tf
from tensorflow import keras

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import numpy.matlib

import datetime


class explicit_operators:
    def linear_slab_1d(f, ws):
        """Implements Q(f,v) = 1/2 int_{-1}^1 f(v') dv' - f(v)."""
        sigma = 1.0 / 2
        I = np.einsum("...i, i -> ...", f, ws)
        Q = sigma * I - f
        return Q


def __parse_cmd_line():
    """Parse command line arguments."""

    parser = ArgumentParser()
    parser.add_argument(
        "model", help="Filename of the saved model.", metavar="MODEL", nargs="?"
    )

    return parser.parse_args()


def __parse_log(filename):
    """Parse log in filename and return Gauss-Legendre quadrature parameters and data."""

    df = pd.read_csv(filename, index_col=0)

    # Cast column names (discrete velocities) to float.
    df.rename(lambda s: float(s), axis=1, inplace=True)

    # Get Gauss-Legendre quadrature parameters and drop the weights from imported data.
    ws = df.iloc[0]
    df.drop(index=df.index[0], inplace=True)

    return ws, df


def __generate_data(vs, ws, Na=101, dt=0.1, Q=explicit_operators.linear_slab_1d):
    """Generate equidistant velocities on [-1, 1] and testcase data for f, Q(f)."""
    As = np.random.uniform(-1, 1, Na).reshape(Na, 1)

    # For t = 0:
    fs = np.exp(vs * As, dtype=np.float32)
    norm_fs = np.einsum("...i, i -> ...", fs, ws).reshape(Na, 1)
    fs = fs / norm_fs
    qs = Q(fs, ws)

    while True:
        for f, q in zip(fs, qs):
            yield f, np.stack([q, np.log(f)], axis=-1)
        fs = fs + dt * qs
        norm_fs = np.einsum("...i, i -> ...", fs, ws).reshape(Na, 1)
        fs = fs / norm_fs
        qs = Q(fs, ws)


def entropydissipation(y_true, y_pred, quadrature=None):
    return tf.math.maximum(
        tf.einsum(
            "...i,...i,...i->...", y_pred[..., 0], y_pred[..., 1], quadrature[1, :]
        ),
        0.0,
    )


def relativemeansquarederror(y_true, y_pred, quadrature=None):
    d = tf.einsum(
        "...i,...i->...", (y_true[..., 0] - y_pred[..., 0]) ** 2, quadrature[1, :]
    )
    a = tf.einsum("...i,...i->...", y_true[..., 0] ** 2, quadrature[1, :])
    return tf.sqrt(d / a)


def massconservation(y_true, y_pred, quadrature=None):
    return tf.math.abs(tf.einsum("...i,...i->...", y_pred[..., 0], quadrature[1, :]))


@keras.utils.register_keras_serializable()
class kernelnet(keras.Model):
    """A kernelnet implementation, aka as the first option."""

    # Variables needed to restore an object.
    __variables = ["beta", "quadrature"]

    @classmethod
    def from_config(cls, config):
        for s in cls.__variables:
            config[s] = keras.utils.deserialize_keras_object(config[s])

        return cls(**config)

    def get_config(self):
        config = super().get_config()
        for s in self.__variables:
            config[s] = getattr(self, s)
        return config

    def __init__(
        self,
        quadrature,
        branch_width=40,
        branch_depth=4,
        trunks_number=10,
        trunk_width=40,
        trunk_depth=4,
        beta=None,
        cholesky_factor=None,
        **kwargs,
    ):
        super().__init__(**kwargs)

        d = 1  # preparation for higher dimension code

        m = len(quadrature[0])
        if not m == len(quadrature[1]):
            raise ValueError(
                "The number of quadrature nodes and the number of weights must be equal."
            )
        self.quadrature = quadrature

        # Branch net:
        if not beta:
            self.beta = keras.models.Sequential()
            self.beta.add(keras.layers.Input(shape=(m,)))
            for n in range(branch_depth - 1):
                self.beta.add(keras.layers.Dense(branch_width, activation="tanh"))

            # Need non-negative output on the last layer for the sspd property.
            self.beta.add(keras.layers.Dense(trunks_number, activation="relu"))
        else:
            self.beta = beta

        if cholesky_factor == None:
            self.cholesky_factor = self.add_weight(shape=(trunks_number, m, m))
        else:
            self.cholesky_factor = cholesky_factor

    def call(self, inputs):
        out = tf.stack(
            [
                tf.einsum(
                    "...i, ...ij -> ...j",
                    self.beta(inputs),
                    tf.einsum(
                        "ijl, l, ikl, k, ...k -> ...ij",
                        self.cholesky_factor,
                        self.quadrature[1, :],
                        self.cholesky_factor,
                        self.quadrature[1, :],
                        inputs,
                        name="Cholesky_integration",
                    ),
                    name="modified-branch-summation",
                ),
                tf.math.log(inputs),
            ],
            axis=-1,
        )
        return out


if __name__ == "__main__":
    args = __parse_cmd_line()
    ##ws, fs = __parse_log(args.filename)

    # dimension
    d = 1
    Nv = 101
    vmax = 1.0
    vs = np.linspace(-vmax, vmax, Nv, dtype=np.float32)
    ws = np.concatenate(([1.0 / 2], np.ones(Nv - 2), [1.0 / 2]), dtype=np.float32)
    quad = tf.stack([vs, ws], name="Quadrature data")

    # training hyperparameters
    epochs = 100
    steps_per_epoch = 2
    batch_size = 10

    shuffle_size = steps_per_epoch * batch_size
    sample_size = epochs * steps_per_epoch * batch_size

    # learning rate
    lr = 0.001

    ds = tf.data.Dataset.from_generator(
        lambda: __generate_data(vs, ws),
        output_signature=(
            tf.TensorSpec(shape=vs.shape, dtype=tf.float32),
            tf.TensorSpec(shape=(Nv, 2), dtype=tf.float32),
        ),
    )

    train_ds = (
        ds.take(sample_size)
        .shuffle(shuffle_size, reshuffle_each_iteration=True)
        .batch(batch_size)
    )
    val_ds = (
        ds.take(sample_size)
        .shuffle(shuffle_size, reshuffle_each_iteration=True)
        .batch(batch_size)
    )

    # comparision
    pred_ds = ds.take(sample_size)
    f_pred = np.zeros(shape=(0, Nv))
    q_true = np.zeros(shape=(0, Nv))
    for s in list(pred_ds.as_numpy_iterator()):
        f_pred = np.vstack((f_pred, s[0]))
        q_true = np.vstack((q_true, s[1][:, 0]))

    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
        log_dir=log_dir, histogram_freq=1
    )

    if args.model:
        model_filename = args.model
        print(f"Loading model from {model_filename}.")
        model = keras.models.load_model(model_filename)
    else:
        print(f"Creating a model from scratch.")
        import time

        model_filename = "kernelnet-" + str(time.time()) + ".keras"
        model = kernelnet(quad)
    optimizer = keras.optimizers.Adam(learning_rate=lr)
    model.compile(
        optimizer=optimizer,
        loss=keras.losses.MeanSquaredError(),
        metrics=[
            keras.metrics.MeanMetricWrapper(
                fn=relativemeansquarederror,
                name="Relative Mean Squared Error",
                quadrature=quad,
            ),
            keras.metrics.MeanMetricWrapper(
                fn=entropydissipation, name="Entropy Dissipation", quadrature=quad
            ),
            keras.metrics.MeanMetricWrapper(
                fn=massconservation, name="Mass Conservation", quadrature=quad
            ),
        ],
    )

    model.fit(  # x_train, y_train,
        train_ds,
        # validation_split=0.5,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        callbacks=[tensorboard_callback],
    )

    print(f"Saving model to {model_filename}.")
    model.save(model_filename, overwrite=True)

    calc_loss = model.evaluate(val_ds, steps=steps_per_epoch)
    print(
        f"MSE loss: {calc_loss[0]}\trMSE: {calc_loss[1]}\tdissipation: {calc_loss[2]}\tmass_growth: {calc_loss[3]}\t"
    )

    # show the predited value vs truth
    q_pred = model.predict(f_pred)

    data_filename = "kernelnet-" + str(time.time()) + ".npz"
    print(f"Saving model predictions to {data_filename}.")
    np.savez(data_filename, vs=vs, f_pred=f_pred, q_true=q_true, q_pred=q_pred[:, :, 0])

    ind = 0
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot(vs, f_pred[ind, :])
    plt.title("f")
    plt.subplot(1, 2, 2)
    plt.plot(vs, q_true[ind, :], label="True")
    plt.plot(vs, q_pred[ind, :, 0], label="Prediction")
    plt.legend(loc="best")
    plt.title("Q")
    plt.show()
