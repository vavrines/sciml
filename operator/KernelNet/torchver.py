# %%
import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import matplotlib.pyplot as plt
import time

class KernelNet(nn.Module):
    """
    A KernelNet implementation in PyTorch, akin to the original Keras version.
    """
    def __init__(
        self,
        quadrature,
        branch_width=40,
        branch_depth=4,
        trunks_number=10,
        trunk_width=40,
        trunk_depth=4,
        beta=None,
        cholesky_factor=None,
    ):
        super(KernelNet, self).__init__()

        m = len(quadrature[0])
        if m != len(quadrature[1]):
            raise ValueError(
                "The number of quadrature nodes and the number of weights must be equal."
            )
        self.quadrature_nodes = torch.tensor(quadrature[0], dtype=torch.float32)
        self.quadrature_weights = torch.tensor(quadrature[1], dtype=torch.float32)

        # Branch net:
        if beta is None:
            layers = [nn.Linear(m, branch_width, bias=True), nn.Tanh()]
            for _ in range(branch_depth - 1):
                layers.append(nn.Linear(branch_width, branch_width, bias=True))
                layers.append(nn.Tanh())
            layers.append(nn.Linear(branch_width, trunks_number, bias=True))
            layers.append(nn.ReLU())  # Ensure non-negative output
            self.beta = nn.Sequential(*layers)
        else:
            self.beta = beta

        if cholesky_factor is None:
            self.cholesky_factor = nn.Parameter(torch.randn(trunks_number, m, m))
        else:
            self.cholesky_factor = cholesky_factor

    def forward(self, inputs):
        beta_output = self.beta(inputs)
        cholesky_integration = torch.einsum(
            "ijl,l,ikl,k,...k->...ij",
            self.cholesky_factor,
            self.quadrature_weights,
            self.cholesky_factor,
            self.quadrature_weights,
            inputs,
        )
        modified_branch_summation = torch.einsum(
            "...i,...ij->...j", beta_output, cholesky_integration
        )
        log_inputs = torch.log(inputs)
        out = torch.stack([modified_branch_summation, log_inputs], dim=-1)
        return out
    

# %%
d = 1
Nv = 101
vmax = 1.0
vs = np.linspace(-vmax, vmax, Nv, dtype=np.float32)
ws = np.concatenate(([1.0 / 2], np.ones(Nv - 2), [1.0 / 2]), dtype=np.float32)
quad = torch.stack([torch.from_numpy(vs), torch.from_numpy(ws)])

# training hyperparameters
epochs = 100
steps_per_epoch = 2
batch_size = int(epochs / 100)

shuffle_size = steps_per_epoch * batch_size
sample_size = epochs * steps_per_epoch * batch_size

# learning rate
lr = 0.001
# %%
m = KernelNet(quad)
# %%
res = m(quad[1])

# %%

# %%
