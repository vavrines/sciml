using NeuralOperators, Flux, CUDA, Plots
import Flux: DataLoader, splitobs, shuffleobs
import WaterLily: AutoBody, Simulation, sim_step!
import ProgressMeter: Progress, next!, @showprogress

"""
Cylinder flow from WaterLily.jl
"""
function circle(n, m; Re=100, U=1)
    radius, center = m / 8.0, m / 2.0
    l = 2.0 * radius
    sdf(x, t) = √sum(abs2, x .- center) - radius

    return Simulation(
        (n, m), # domain size 
        (U, 0), # domain velocity (& velocity scale)
        l; # length scale
        ν=U * l / Re, # fluid viscosity 
        body=AutoBody(sdf), # geometry
    )
end

function gen_data(ts::AbstractRange)
    @info "Generating data... "
    p = Progress(length(ts))

    n, m = 3(2^5), 2^6
    circ = circle(n, m)

    𝐩s = Array{Float32}(undef, 1, n, m, length(ts))
    for (i, t) in enumerate(ts)
        sim_step!(circ, t)
        𝐩s[1, :, :, i] .= Float32.(circ.flow.p)[2:(end-1), 2:(end-1)]

        next!(p)
    end

    return 𝐩s
end

function get_mno_dataloader(;
    ts::AbstractRange=LinRange(100, 11000, 10000),
    ratio::Float64=0.95,
    batchsize=100,
)
    data = gen_data(ts)
    𝐱, 𝐲 = data[:, :, :, 1:(end-1)], data[:, :, :, 2:end]
    n = length(ts) - 1

    data_train, data_test = splitobs(shuffleobs((𝐱, 𝐲)); at=ratio)

    loader_train = DataLoader(data_train; batchsize=batchsize, shuffle=true)
    loader_test = DataLoader(data_test; batchsize=batchsize, shuffle=false)

    return loader_train, loader_test
end

ts = LinRange(100, 110, 100)
data = get_mno_dataloader(; ts=ts)

model = MarkovNeuralOperator(; ch=(1, 64, 64, 64, 64, 64, 1), modes=(24, 24), σ=gelu)
#data_train = data[1]
#data_test = data[2]
#model(data_train.data[1])

#--- default FluxTraining trainer ---#
import FluxTraining

optimiser = Flux.Optimiser(WeightDecay(1.0f-4), Flux.Adam(1.0f-3))
loss_func = l₂loss
device = gpu
learner = FluxTraining.Learner(
    model,
    data,
    optimiser,
    loss_func,
    FluxTraining.ToDevice(device, device),
    FluxTraining.Checkpointer(joinpath(@__DIR__, "./model/")),
)

FluxTraining.fit!(learner, 10)

#--- hand-written trainer ---#
data_train = data[1]
X, Y = data_train.data
L = size(X, 4)
loss(x, y) = sum(abs2, model(x) - y) / L
cb = () -> println("loss: $(loss(X, Y))")

model1 = model |> device
data1 = data_train |> device

opt_state = Flux.setup(Adam(3e-4), model1)
for iter in 1:10
    for (x, y) in data1
        # Compute the loss and the gradients:
        l, gs = Flux.withgradient(m -> l₂loss(m(x), y), model1)
        # Update the model parameters (and the Adam momenta):
        Flux.update!(opt_state, model1, gs[1])

        println("loss = $l")
    end
end

#--- Flux trainer ---#
for iter in 1:10
    Flux.train!(model1, data1, opt_state) do m, x, y
        @show l₂loss(m(x), y)
    end
end
