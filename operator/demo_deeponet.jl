using Lux, NeuralOperators, Random

branch_net = Chain(Dense(64 => 32), Dense(32 => 32), Dense(32 => 16))
trunk_net = Chain(Dense(1 => 8), Dense(8 => 8), Dense(8 => 16))
deeponet = DeepONet(branch_net, trunk_net)
ps, st = Lux.setup(Random.default_rng(), deeponet)

# 5 samples
u = rand(Float32, 64, 1) # u is represented by 64 sensors
y = rand(Float32, 1, 10, 1) # input vector of dimension 10

# prediction
pred = deeponet((u, y), ps, st)[1] # shape: 10 × 5

# internal pass
b = branch_net(u, ps.branch, st.branch)[1]
t = trunk_net(y, ps.trunk, st.trunk)[1]

br = reshape(b, size(b, 1), 1, size(b, 2))
bt = sum(br .* t; dims=1)
pred1 = dropdims(bt; dims=1)
