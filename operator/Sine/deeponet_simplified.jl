using NeuralOperators, Lux, Solaris, Plots

T = Float32
nx = 32
nd = 100

grid = range(0, 2π; length=nx) .|> T
α = 0.5 .+ 0.5 .* rand(nd) .|> T

utrain = zeros(T, nx, nd)
for i in 1:nd, j in 1:nx
    utrain[j, i] = sin(α[i] * grid[j])
end

soltrain = zeros(T, nd, nx)
for i in 1:nd, j in 1:nx
    soltrain[i, j] = 1 / α[i] * sin(α[i] * grid[j])
end

ytrain = zeros(T, 1, nx)
ytrain[1, :] .= grid

model = DeepONet(;
    branch=(nx, nx, nx, nx),
    trunk=(1, nx, nx, nx),
    branch_activation=gelu,
    trunk_activation=gelu,
)

ps, st = SR.setup(model)
pv = ComponentArray(ps)

function loss(p)
    pred = SR.infer_deeponet(model, p, st, utrain, ytrain)
    err = pred .- soltrain
    l = sum(abs2, err) / size(err, 2)

    return l
end

res = sci_train(loss, pv, Adam(0.0001); cb=default_callback, maxiters=1000, ad=AutoZygote())
res = sci_train(
    loss,
    res.u,
    AdamW(0.0001);
    cb=default_callback,
    maxiters=1000,
    ad=AutoZygote(),
)

pred = SR.infer_deeponet(model, res.u, st, utrain, ytrain)
let idx = 12
    plot(pred[idx, :]; label="nn")
    plot!(soltrain[idx, :]; label="theory")
end
