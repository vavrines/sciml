using NeuralOperators, Lux, Solaris, Plots

function OperatorNet(;
    branch,
    trunk,
    branch_activation=identity,
    trunk_activation=identity,
    additional=NoOpLayer(),
)
    branch_net = Chain([
        Dense(
            branch[i] => branch[i+1],
            ifelse(i == length(branch) - 1, identity, branch_activation),
        ) for i in 1:(length(branch)-1)
    ]...)

    trunk_net = Chain([
        Dense(
            trunk[i] => trunk[i+1],
            ifelse(i == length(trunk) - 1, identity, trunk_activation),
        ) for i in 1:(length(trunk)-1)
    ]...)

    return DeepONet(branch_net, trunk_net, additional)
end

function infer_nomad(model, ps, st, u, y)
    bs = model.branch(u, ps.branch, st.branch)[1]
    ts = cat(bs, y; dims=1)
    pred = model.trunk(ts, ps.trunk, st.trunk)[1]

    return pred
end

#--- train ---#
T = Float32
nx = 32
nd = 100

grid = range(0, 2π; length=nx) .|> T
α = 0.5 .+ 0.5 .* rand(nd) .|> T

utrain = zeros(T, nx, nd)
for i in 1:nd, j in 1:nx
    utrain[j, i] = sin(α[i] * grid[j])
end

# here the location of ytrain can vary, but the shape needs to be the same as `nd`
ytrain = zeros(T, 1, nd)
ytrain[1, :] .= range(0, 2π; length=nd) .|> T

soltrain = zeros(T, 1, nd)
for i in 1:nd
    soltrain[1, i] = 1 / α[i] * sin(α[i] * ytrain[i])
end

model = OperatorNet(;
    branch=(nx, 8, 8, 7),
    trunk=(8, 4, 4, 1),
    branch_activation=gelu,
    trunk_activation=gelu,
)
ps, st = SR.setup(model)
pv = ComponentArray(ps)

function loss(p)
    pred = infer_nomad(model, p, st, utrain, ytrain)
    err = pred .- soltrain
    l = sum(abs2, err) / size(err, 2)

    return l
end

res = sci_train(loss, pv, Adam(0.0001); cb=default_callback, maxiters=1000, ad=AutoZygote())
res = sci_train(
    loss,
    res.u,
    AdamW(0.0001);
    cb=default_callback,
    maxiters=10000,
    ad=AutoZygote(),
)

#--- test ---#
β = 0.5 + 0.5 * rand()
utest = zeros(T, nx, 1)
for j in 1:nx
    utest[j] = sin(β * grid[j])
end

nx1 = 72
ytest = range(0, 2π; length=nx1) .|> T |> permutedims

soltest = zeros(T, nx1)
for i in 1:nx1
    soltest[i] = 1 / β * sin(β * ytest[i])
end

pred = zeros(nx1)
_y = Matrix{Float64}(undef, 1, 1)
for i in eachindex(pred)
    _y[1] = ytest[i]
    pred[i] = infer_nomad(model, res.u, st, utest, _y)[1]
end

plot(pred; label="nn")
plot!(soltest; label="theory")
