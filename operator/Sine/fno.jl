using NeuralOperators, Lux, Solaris, CairoMakie

T = Float32
data_size = 128
m = 32

xrange = range(0, 2π; length=m) .|> T
α = 0.5f0 .+ 0.5f0 .* rand(T, data_size)

u_data = zeros(T, m, 1, data_size)
v_data = zeros(T, m, 1, data_size)
for i in 1:data_size
    u_data[:, 1, i] .= sin.(α[i] .* xrange)
    v_data[:, 1, i] .= -inv(α[i]) .* cos.(α[i] .* xrange)
end

fno = FourierNeuralOperator(
    gelu;                    # activation function
    chs=(1, 64, 64, 128, 1), # channel weights
    modes=(16,),             # number of Fourier modes to retain
    permuted=Val(true),       # structure of the data means that columns are observations
)

ps, st = SR.setup(fno)
data = [(u_data, v_data)]

function train!(model, ps, st, data; epochs=1)
    # Initialize a training state and an optimizer (Adam, in this case).
    tstate = Training.TrainState(model, ps, st, Adam(0.01f0))
    # Loop over epochs, then loop over each batch of training data, and step into the training:
    for _ in 1:epochs
        for (x, y) in data
            _, loss, _, tstate =
                Training.single_train_step!(AutoZygote(), MSELoss(), (x, y), tstate)
            println("loss: $loss")
        end
    end
    return tstate
end

ts = train!(fno, ps, st, data; epochs=500)

input_data = u_data[:, 1, 1]
reshaped_input = reshape(input_data, length(input_data), 1, 1)
output_data = fno(reshaped_input, ps, st)[1]

begin
    f, a, p = lines(xrange, dropdims(reshaped_input; dims=(2, 3)); label="u")
    scatter!(a, xrange, dropdims(output_data; dims=(2, 3)); label="nn")
    lines!(a, xrange, v_data[:, 1, 1]; label="theory")
    axislegend(a)
    f
end
