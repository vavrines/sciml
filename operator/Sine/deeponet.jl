"""
Given the sine function ``u(x)=sin(αx)``,
we want to learn ``G: u→v``,
such that ``v(x)=du/dx ∀x∈[0,2π], α∈[0.5,1]``.

The neural operator is defined as ``Gu(y)``,
which maps the initial condition to its derivatives.
"""

using NeuralOperators, Lux, Solaris, Plots

T = Float32
nx = 32 # grid size
nd = 100 # training data size

grid = range(0, 2π; length=nx) .|> T
α = 0.5 .+ 0.5 .* rand(nd) .|> T

# initial condition
utrain = zeros(T, nx, nd, 1)
for i in 1:nd, j in 1:nx
    utrain[j, i, 1] = sin(α[i] * grid[j])
end

# target solution
soltrain = zeros(T, nd, nx)
for i in 1:nd, j in 1:nx
    soltrain[i, j] = 1 / α[i] * sin(α[i] * grid[j])
end

# grid sensor
ytrain = zeros(T, 1, nx, 1)
ytrain[1, :, 1] .= grid

model = DeepONet(;
    branch=(nx, nx, nx, nx),
    trunk=(1, nx, nx, nx),
    branch_activation=gelu,
    trunk_activation=gelu,
)

ps, st = SR.setup(model)
p0 = ComponentArray(ps)

function loss(p)
    pred = model((utrain, ytrain), p, st)[1][:, :, 1] .- soltrain
    l = sum(abs2, pred) / size(pred, 2)

    return l
end

res = sci_train(loss, p0, Adam(0.0001); cb=default_callback, maxiters=1000, ad=AutoZygote())
res = sci_train(
    loss,
    res.u,
    AdamW(0.0001);
    cb=default_callback,
    maxiters=1000,
    ad=AutoZygote(),
)

pred = first(model((utrain, ytrain), res.u, st))
let idx = 5
    plot(pred[idx, :, 1]; label="nn")
    plot!(soltrain[idx, :]; label="theory")
end
