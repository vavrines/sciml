using NeuralOperators, Lux, LuxCUDA, Plots, MAT, Solaris

cd(@__DIR__)
const T = Float32
device = SR.gpu
#device = SR.cpu

vars = matread("burgers_data_R10.mat")

ntrain = 1000
ntest = 100
subsample = 2^3

xtrain = vars["a"][1:ntrain, 1:subsample:end] |> permutedims .|> T |> device
ytrain = vars["u"][1:ntrain, 1:subsample:end] .|> T |> device

ngrid = size(ytrain, 2)
grid = collect(range(0, 1; length=ngrid)) |> permutedims .|> T |> device

model = DeepONet(;
    branch=(ngrid, ngrid, ngrid, ngrid),
    trunk=(1, ngrid, ngrid, ngrid),
    branch_activation=gelu,
    trunk_activation=gelu,
)
ps, st = SR.setup(model) |> device
pv = ComponentArray(SR.cpu(ps)) |> device

function inference(p; u=xtrain, y=grid)
    bs = model.branch(u, p.branch, st.branch)[1]
    ts = model.trunk(y, p.trunk, st.trunk)[1]
    pred = permutedims(bs) * ts

    return pred
end

function loss(p)
    pred = inference(p)
    err = pred .- ytrain
    l = sum(abs2, err) / size(err, 2)

    return l
end

res = sci_train(
    loss,
    pv,
    Adam(0.0001);
    device=device,
    cb=default_callback,
    maxiters=1000,
    ad=AutoZygote(),
)

res = sci_train(
    loss,
    res.u,
    AdamW(0.0001);
    device=device,
    cb=default_callback,
    maxiters=5000,
    ad=AutoZygote(),
)

pred = inference(res.u; u=xtrain, y=grid) |> SR.cpu

let idx = rand(1:size(ytrain, 2))
    plot(pred[idx, :])
    plot!(SR.cpu(ytrain[idx, :]))
end
