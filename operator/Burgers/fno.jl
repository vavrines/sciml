"""
OperatorLearning.jl's tutorial with incorporation of NeuralOperators.jl

## data
- `a`: initial condition
- `u`: final solution

There are 2048 different initial conditions provided in the dataset. We randomly choose 1000 for training and 100 for testing.
The grid size is 8192 ranging [0, 1]. In the training, we reduce it to 1024 for the efficiency.
"""

using NeuralOperators, Flux, Plots, MAT
using Solaris: @epochs

cd(@__DIR__)
device = gpu
#device = cpu

# Read the data from MAT file and store it in a dict
vars = matread("burgers_data_R10.mat") |> device

# For trial purposes, we might want to train with different resolutions
# So we sample only every n-th element
subsample = 2^3

# create the x training array, according to our desired grid size
xtrain = vars["a"][1:1000, 1:subsample:end] |> device
# create the x test array
xtest = vars["a"][(end-99):end, 1:subsample:end] |> device

# Create the y training array
ytrain = vars["u"][1:1000, 1:subsample:end] |> device
# Create the y test array
ytest = vars["u"][(end-99):end, 1:subsample:end] |> device

# The data is missing grid data, so we create it
# `collect` converts data type `range` into an array
grid = collect(range(0, 1; length=length(xtrain[1, :]))) |> device

# Merge the created grid with the data
# Output has the dims: batch x grid points x 2  (a(x), x)
# First, reshape the data to a 3D tensor,
# Then, create a 3D tensor from the synthetic grid data
# and concatenate them along the newly created 3rd dim
xtrain =
    cat(
        reshape(xtrain, (1000, 1024, 1)),
        reshape(repeat(grid, 1000), (1000, 1024, 1));
        dims=3,
    ) |> device
ytrain =
    cat(
        reshape(ytrain, (1000, 1024, 1)),
        reshape(repeat(grid, 1000), (1000, 1024, 1));
        dims=3,
    ) |> device
# Same treatment with the test data
xtest =
    cat(
        reshape(xtest, (100, 1024, 1)),
        reshape(repeat(grid, 100), (100, 1024, 1));
        dims=3,
    ) |> device
ytest =
    cat(
        reshape(ytest, (100, 1024, 1)),
        reshape(repeat(grid, 100), (100, 1024, 1));
        dims=3,
    ) |> device

# Our net wants the input in the form (2,grid,batch), though,
# So we permute
xtrain, xtest = permutedims(xtrain, (3, 2, 1)), permutedims(xtest, (3, 2, 1)) |> device
ytrain, ytest = permutedims(ytrain, (3, 2, 1)), permutedims(ytest, (3, 2, 1)) |> device

train_loader = Flux.Data.DataLoader((xtrain, ytrain); batchsize=20, shuffle=true) |> device
test_loader = Flux.Data.DataLoader((xtest, ytest); batchsize=20, shuffle=false) |> device

#--- NeuralOperators.jl ---#
model =
    Chain(
        # lift (d + 1)-dimensional vector field to n-dimensional vector field
        # here, d == 1 and n == 64
        Dense(2, 128),
        # map each hidden representation to the next by integral kernel operator
        OperatorKernel(128 => 128, (16,), FourierTransform, gelu),
        OperatorKernel(128 => 128, (16,), FourierTransform, gelu),
        OperatorKernel(128 => 128, (16,), FourierTransform, gelu),
        OperatorKernel(128 => 128, (16,), FourierTransform),
        # project back to the scalar field of interest space
        Dense(128, 128, gelu),
        Dense(128, 2),
    ) |> device

"""
#--- OperatorLearning.jl ---#
# Set up the Fourier Layer
# 128 in- and outputs, batch size 20 as given above, grid size 1024
# 16 modes to keep, σ activation on the gpu
layer = FourierLayer(128, 128, 1024, 16, gelu, bias_fourier = false) |> device

# The whole architecture
# linear transform into the latent space, 4 Fourier Layers,
# then transform it back
model =
    Chain(
        Dense(2, 128; bias = false),
        layer,
        layer,
        layer,
        layer,
        Dense(128, 2; bias = false),
    ) |> device
"""

learning_rate = 0.001
opt = ADAM(learning_rate)

# Specify the model parameters
parameters = Flux.params(model)

# The loss function
loss(x, y) = Flux.Losses.mse(model(x), y)

# Define a callback function that gives some output during training
evalcb() = @show(loss(xtest, ytest))
# Print the callback only every 5 seconds, 
throttled_cb = Flux.throttle(evalcb, 5)

# Do the training loop
@epochs 20 Flux.train!(loss, parameters, train_loader, opt, cb=throttled_cb)

loss(xtrain, ytrain)
loss(xtest, ytest)
