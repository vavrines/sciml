using NeuralOperators, Lux, LuxCUDA, Plots, MAT, Random
import Solaris: @epochs, Adam, cpu, gpu

cd(@__DIR__)
rng = Random.default_rng(7)

const T = Float32
device = gpu
#device = cpu

# Read the data from MAT file and store it in a dict
vars = matread("burgers_data_R10.mat")

# For trial purposes, we might want to train with different resolutions
# So we sample only every n-th element
subsample = 2^3

ntrain = 1000
ntest = 100

# Create the x training array, according to our desired grid size
xtrain = vars["a"][1:ntrain, 1:subsample:end] |> permutedims .|> T
xtrain = reshape(xtrain, 1024, :, 1) |> device

# Create the x test array
xtest = vars["a"][(end-ntest+1):end, 1:subsample:end] |> permutedims .|> T
xtest = reshape(xtest, 1024, :, 1) |> device

# Create the y training array
ytrain = vars["u"][1:ntrain, 1:subsample:end] .|> T |> device

# Create the y test array
ytest = vars["u"][(end-ntest+1):end, 1:subsample:end] .|> T |> device

# The data is missing grid data, so we create it
grid = collect(range(0, 1; length=1024)) |> permutedims .|> T
grid = reshape(grid, 1, :, 1) |> device

# Create the DeepONet:
# IC is given on grid of 1024 points, and we solve for a fixed time t in one
# spatial dimension x, making the branch input of size 1024 and trunk size 1
# We choose GeLU activation for both subnets
model = DeepONet(;
    branch=(1024, 1024, 1024, 1024),
    trunk=(1, 1024, 1024, 1024),
    branch_activation=gelu,
    trunk_activation=gelu,
)
# This can be written alternatively as:
#model = begin
#    branch = Chain(Dense(1024 => 1024, tanh), Dense(1024 => 1024, tanh), Dense(1024 => 1024))
#    trunk = Chain(Dense(1 => 1024, tanh), Dense(1024 => 1024, tanh), Dense(1024 => 1024))
#    DeepONet(branch, trunk)
#end
ps, st = Lux.setup(rng, model) |> device

# We use the ADAM optimizer for training
learning_rate = 0.001
opt = Adam(learning_rate)

function loss_fn(model, ps, st, ((v, y), u))
    û, stₙ = model((v, y), ps, st)
    return MAELoss()(û, u), stₙ, (;)
end

function train_model!(model, ps, st, data; epochs=1)
    train_state = Training.TrainState(model, ps, st, Adam(0.0001f0))

    for epoch in 1:epochs
        _, loss, _, train_state =
            Training.single_train_step!(AutoZygote(), loss_fn, data, train_state)

        println("epoch: $(epoch), loss: $(loss)")
    end

    return train_state.parameters, train_state.states
end

ps, st = train_model!(model, ps, st, ((xtrain, grid), ytrain); epochs=500)
ps, st = train_model!(model, ps, st, ((xtrain, grid), ytrain); epochs=1000)

using CairoMakie

pred = first(model((xtrain, grid), ps, st)) |> cpu

begin
    fig = Figure(; size=(1024, 1024))

    axs = [Axis(fig[i, j]) for i in 1:4, j in 1:4]
    for i in 1:4, j in 1:4
        idx = i + (j - 1) * 4
        ax = axs[i, j]
        l1 = lines!(ax, vec(cpu(grid)), pred[idx, :, 1])
        l2 = lines!(ax, vec(cpu(grid)), cpu(ytrain)[idx, :, 1])

        i == 4 && (ax.xlabel = "x")
        j == 1 && (ax.ylabel = "u(x)")

        if i == 1 && j == 1
            axislegend(ax, [l1, l2], ["Predictions", "Ground Truth"])
        end
    end
    linkaxes!(axs...)

    fig[0, :] = Label(fig, "Burgers Equation using DeepONet"; tellwidth=false, font=:bold)

    fig
end
