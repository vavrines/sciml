using KitBase, Plots
using Base.Threads: @threads

cd(@__DIR__)
include("gks_flux.jl")

function prepare_ics(kn, ρr, tr; set, ps, vs)
    gas = Gas(; Kn=kn)

    primL = [1.0, 0.0, 0.5]
    primR = [ρr, 0.0, 0.5 / tr]
    wL = prim_conserve(primL, 5 / 3)
    wR = prim_conserve(primR, 5 / 3)

    fw = function (x, p)
        if x <= 0.5
            return wL
        else
            return wR
        end
    end
    bc = function (x, p)
        if x <= 0.5
            return primL
        else
            return primR
        end
    end
    ff = function (x, p)
        prim = bc(x, p)
        h = maxwellian(vs.u, prim)
        b = h ./ prim[end]
        return h, b
    end
    p = ()

    fw, ff, bc, p = KB.config_ib(set, ps, vs, gas)
    ib = IB2F(fw, ff, bc, p)

    return gas, ib
end

set = Setup(; case="sod", space="1d2f1v", maxTime=0.12)
ps = PSpace1D(0.0, 1.0, 200, 1)
vs = VSpace1D(-5.0, 5.0, 100)

kn, ρr, tr = 1e-5, 0.125, 0.8

gas, ib = prepare_ics(kn, ρr, tr; set=set, ps=ps, vs=vs)

ks = SolverSet(set, ps, vs, gas, ib)
ctr, face = init_fvm(ks, :static_array; structarray=false)

t = 0.0
dt = KB.timestep(ks, ctr, t)
nt = ks.set.maxTime ÷ dt |> Int
res = zeros(3)

for iter in 1:nt
    reconstruct!(ks, ctr)

    @inbounds @threads for i in 1:ks.ps.nx+1
        wL = ctr[i-1].w .+ 0.5 * ps.dx[i-1] .* ctr[i-1].sw
        wR = ctr[i].w .- 0.5 * ps.dx[i-1] .* ctr[i].sw

        flux_ref!(
            face[i].fw,
            wL,
            wR,
            ks.gas.K,
            ks.gas.γ,
            ks.gas.μᵣ,
            ks.gas.ω,
            dt,
            ctr[i-1].sw,
            ctr[i].sw,
        )
    end

    update!(ks, ctr, face, dt, res)
end

plot(ks, ctr)

sol = extract_sol(ks, ctr)
@. sol[:, end] = 1 / sol[:, end]
