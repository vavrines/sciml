import math

def exact_riemann_solver(left_state, right_state, t, x, gamma=1.4):
    """
    Exact Riemann solver for the Euler equations.
    
    Parameters:
      left_state  -- tuple (rho_L, u_L, p_L)
      right_state -- tuple (rho_R, u_R, p_R)
      t           -- time at which to evaluate the solution (t > 0)
      x           -- spatial coordinate where the solution is desired
      gamma       -- ratio of specific heats (default 1.4)
      
    Returns:
      A tuple (rho, u, p) representing the density, velocity, and pressure at (t, x)
    """
    rhoL, uL, pL = left_state
    rhoR, uR, pR = right_state
    
    # At t = 0, return the initial condition (discontinuous data)
    if t == 0:
        return left_state if x < 0 else right_state

    # Compute sound speeds in left and right states
    cL = math.sqrt(gamma * pL / rhoL)
    cR = math.sqrt(gamma * pR / rhoR)
    
    # f-function for a given state (shock or rarefaction)
    def f(p, rho, p_i, c):
        if p > p_i:  # shock wave
            A = 2.0 / ((gamma+1) * rho)
            B = (gamma-1) / (gamma+1) * p_i
            return (p - p_i) * math.sqrt(A / (p + B))
        else:  # rarefaction wave
            return 2 * c / (gamma - 1) * ((p/p_i)**((gamma-1)/(2*gamma)) - 1)

    # Derivative of the f-function with respect to p
    def df(p, rho, p_i, c):
        if p > p_i:  # shock
            A = 2.0 / ((gamma+1) * rho)
            B = (gamma-1) / (gamma+1) * p_i
            sqrt_term = math.sqrt(A / (p + B))
            return sqrt_term * (1 - 0.5*(p - p_i)/(p + B))
        else:  # rarefaction
            return (1/(rho*c)) * (p/p_i)**(- (gamma+1)/(2*gamma))
    
    # --- Find p_star using Newton's method ---
    p_old = 0.5*(pL + pR)  # initial guess
    for _ in range(20):
        fL = f(p_old, rhoL, pL, cL)
        fR = f(p_old, rhoR, pR, cR)
        func = fL + fR + (uR - uL)
        dfL = df(p_old, rhoL, pL, cL)
        dfR = df(p_old, rhoR, pR, cR)
        dp = - func / (dfL + dfR)
        p_new = p_old + dp
        if p_new < 0:
            p_new = 1e-6  # enforce positive pressure
        if abs(dp) < 1e-6:
            break
        p_old = p_new
    p_star = p_old
    u_star = 0.5*(uL + uR) + 0.5*(f(p_star, rhoR, pR, cR) - f(p_star, rhoL, pL, cL))
    
    # --- Compute wave speeds ---
    # Left wave speeds
    if p_star > pL:  # left shock
        SL = uL - cL * math.sqrt((gamma+1)/(2*gamma)*(p_star/pL) + (gamma-1)/(2*gamma))
    else:  # left rarefaction
        SHL = uL - cL               # head of rarefaction
        STL = u_star - cL * (p_star/pL)**((gamma-1)/(2*gamma))  # tail of rarefaction
        
    # Right wave speeds
    if p_star > pR:  # right shock
        SR = uR + cR * math.sqrt((gamma+1)/(2*gamma)*(p_star/pR) + (gamma-1)/(2*gamma))
    else:  # right rarefaction
        SHR = uR + cR               # head of rarefaction
        STR = u_star + cR * (p_star/pR)**((gamma-1)/(2*gamma))  # tail of rarefaction
        
    xi = x / t  # self-similar variable
    
    # --- Determine the solution at (t, x) based on xi ---
    if xi < u_star:  # solution from the left side
        if p_star > pL:  # left shock
            if xi < SL:
                return left_state
            else:
                # state in the left star region (behind the shock)
                rho_star_L = rhoL * ((p_star/pL) + (gamma-1)/(gamma+1)) / (((gamma-1)/(gamma+1))*(p_star/pL) + 1)
                return (rho_star_L, u_star, p_star)
        else:  # left rarefaction
            if xi < SHL:
                return left_state
            elif xi > STL:
                # state in the left star region (after the fan)
                rho_star_L = rhoL * (p_star/pL)**(1/gamma)
                return (rho_star_L, u_star, p_star)
            else:
                # within the rarefaction fan
                u = (2/(gamma+1)) * (cL + (gamma-1)/2 * uL + xi)
                c = (2/(gamma+1)) * (cL + (gamma-1)/2 * (uL - xi))
                rho = rhoL * (c/cL)**(2/(gamma-1))
                p = pL * (c/cL)**(2*gamma/(gamma-1))
                return (rho, u, p)
    else:  # solution from the right side
        if p_star > pR:  # right shock
            if xi > SR:
                return right_state
            else:
                rho_star_R = rhoR * ((p_star/pR) + (gamma-1)/(gamma+1)) / (((gamma-1)/(gamma+1))*(p_star/pR) + 1)
                return (rho_star_R, u_star, p_star)
        else:  # right rarefaction
            if xi > SHR:
                return right_state
            elif xi < STR:
                # state in the right star region (before the fan ends)
                rho_star_R = rhoR * (p_star/pR)**(1/gamma)
                return (rho_star_R, u_star, p_star)
            else:
                # within the rarefaction fan on the right
                u = (2/(gamma+1)) * (-cR + (gamma-1)/2 * uR + xi)
                c = (2/(gamma+1)) * (cR - (gamma-1)/2 * (xi - uR))
                rho = rhoR * (c/cR)**(2/(gamma-1))
                p = pR * (c/cR)**(2*gamma/(gamma-1))
                return (rho, u, p)

def generalized_riemann_solver(left_state, left_slope, right_state, right_slope, t, x, gamma=1.4):
    """
    Generalized Riemann solver for the Euler equations that accounts for local slopes 
    in the primitive variables (density, velocity, pressure). This predictor-corrector 
    style approach uses a piecewise linear reconstruction to extrapolate left/right 
    states to the interface before solving the Riemann problem.
    
    Parameters:
      left_state   -- tuple (rho_L, u_L, p_L) at the left cell center
      left_slope   -- tuple (drho/dx)_L, (du/dx)_L, (dp/dx)_L computed in the left cell
      right_state  -- tuple (rho_R, u_R, p_R) at the right cell center
      right_slope  -- tuple (drho/dx)_R, (du/dx)_R, (dp/dx)_R computed in the right cell
      t            -- time at which to evaluate the solution (t > 0)
      x            -- spatial coordinate where the solution is desired
      gamma        -- ratio of specific heats (default 1.4)
      
    Returns:
      A tuple (rho, u, p) representing the solution at (t, x).
    
    Notes:
      1. In a MUSCL-type finite volume scheme, one would typically reconstruct
         the left/right states at an interface (often located at x = 0) using the slopes.
      2. Here we use a simple extrapolation over a distance ~ ±(Δt/2). More sophisticated 
         approaches may include additional temporal derivative corrections computed from 
         the governing equations.
    """
    # For a predictor step, we assume a half time-step extrapolation.
    # (This corresponds to extrapolating from the cell centers to the interface.)
    dt = t  # In practice, one might use dt/2 for a predictor; here we simply use t for demonstration.
    
    # Define the distances from the cell centers to the interface.
    # For the left cell, we extrapolate forward (to the right) by dx = +Δx/2,
    # and for the right cell, we extrapolate left by dx = -Δx/2.
    # Here we approximate the distance using a half time-step multiplied by a characteristic speed.
    # A common choice in a predictor-corrector scheme is to use dx = ±0.5 * dt.
    dx_left = 0.5 * dt   # left cell interface extrapolation (to the right)
    dx_right = -0.5 * dt # right cell interface extrapolation (to the left)
    
    # Unpack states and slopes
    rhoL, uL, pL = left_state
    drhoL, duL, dpL = left_slope
    rhoR, uR, pR = right_state
    drhoR, duR, dpR = right_slope
    
    # Extrapolate the states from the cell centers to the interface (x=0)
    left_state_ex = (rhoL + drhoL * dx_left,
                     uL + duL * dx_left,
                     pL + dpL * dx_left)
    right_state_ex = (rhoR + drhoR * dx_right,
                      uR + duR * dx_right,
                      pR + dpR * dx_right)
    
    # Solve the standard Riemann problem with the extrapolated (interface) states.
    solution = exact_riemann_solver(left_state_ex, right_state_ex, t, x, gamma)
    
    return solution

# Example usage:
if __name__ == "__main__":
    # Cell-center states (density, velocity, pressure)
    left_state  = (1.0, 0.0, 1.0)    # left cell center
    right_state = (0.125, 0.0, 0.1)   # right cell center

    # Slopes computed in each cell (for instance, by a slope limiter)
    # These values are just for illustration.
    left_slope  = (0.1, 0.0, -0.05)
    right_slope = (-0.05, 0.0, 0.02)

    # Choose the time and location at which to evaluate the solution.
    #t = 0.25
    t = 0.5
    x = 0.0  # interface location
    
    state = generalized_riemann_solver(left_state, left_slope, right_state, right_slope, t, x)
    print("At (t, x) = ({}, {}), solution (density, velocity, pressure) = {}".format(t, x, state))
